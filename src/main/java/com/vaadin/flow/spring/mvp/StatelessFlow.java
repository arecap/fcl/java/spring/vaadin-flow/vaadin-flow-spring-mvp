package com.vaadin.flow.spring.mvp;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface StatelessFlow {

    String value();

}
