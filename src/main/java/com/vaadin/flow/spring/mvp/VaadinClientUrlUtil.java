package com.vaadin.flow.spring.mvp;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;

public final class VaadinClientUrlUtil {

    public static final String getProtocol() {
        return (((VaadinServletRequest) VaadinService.getCurrentRequest()).getProtocol().toLowerCase().contains("https") ? "https" : "http");
    }

    public static int getPort() {
        return ((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerPort();
    }

    public static final String getDomainPath() {
        return getProtocol()+"//"+((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerName() +
                (getPort() == 80 ? "" : ":" + ((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerPort());
    }

    public static final String getRoutePath() {
        return getDomainPath() + "/" + UI.getCurrent().getInternals().getLastHandledLocation().getPathWithQueryParameters();
    }

}
