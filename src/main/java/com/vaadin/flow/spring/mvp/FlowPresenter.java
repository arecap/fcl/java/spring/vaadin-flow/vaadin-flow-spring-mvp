package com.vaadin.flow.spring.mvp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.io.Serializable;
import java.util.EventObject;
import java.util.Optional;

public interface FlowPresenter<V extends FlowView> extends Serializable {

	default Logger getLogger() {
		return LoggerFactory.getLogger(getClass());
	}

	default void prepareModelAndView(EventObject event) {
		getLogger().info("prepareModelAndView");
		Optional<StatelessFlow> statelessFlow = Optional
				.ofNullable(AnnotatedElementUtils.getMergedAnnotation(getClass(), StatelessFlow.class));
		if(statelessFlow.isPresent()) {
			resolveApplicationState(statelessFlow.get().value());
		}
		prepareModel(event);

		getView().beforePrepareView(event);
		getView().prepareView();
		afterPrepareModel(event);
		getView().afterPrepareView();
	}

	void setView(V view);

	V getView();

	default  void beforeLeavingView(EventObject event) {
		getLogger().info("beforeLeavingView");
	}

	default void prepareModel(EventObject event) {
		getLogger().info("prepareModel");
	}

	default  void afterPrepareModel(EventObject event) {
		getLogger().info("afterPrepareModel");
	}

	default void resolveApplicationState(String state) {
		getLogger().info("resolveApplicationState:\t"+state);
		getLogger().info("resolve path:\t"+StateEngineUtil.encodePresenterState(this));
	}


}
