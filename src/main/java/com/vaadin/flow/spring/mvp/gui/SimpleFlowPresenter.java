package com.vaadin.flow.spring.mvp.gui;

import com.vaadin.flow.spring.mvp.FlowPresenter;
import com.vaadin.flow.spring.mvp.FlowView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus.UIEventBus;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.EventObject;

public abstract class SimpleFlowPresenter<V extends FlowView> implements FlowPresenter<V> {
	
	private static final long serialVersionUID = 8713397848603426939L;

	private V view;
	
	@Autowired
    private UIEventBus uIEventBus;
	
	@PostConstruct
    private void initEventBus() {
		getLogger().info("Presenter ui event bus subscribe");
    	uIEventBus.subscribe(this);
    }
    
    @PreDestroy
    private void predestroyEventBus() {
		getLogger().info("Presenter ui event bus unsubscribe");
    	uIEventBus.unsubscribe(this);
    }

	@Override
	public void setView(V view) {
		getLogger().info("Presenter set view of type " + view.getClass());
		this.view = view;
	}

	@Override
	public V getView() {
		return view;
	}

	public void publish(EventObject eventObject) {
		getLogger().info("Presenter send event of type " + eventObject.getClass());
		uIEventBus.publish(this, eventObject);
	}

}
