# Vaadin Flow Spring Microservices Model View Presenter

Vaadin Spring Add-on enable Model View Presenter design in Vaadin Spring UIScope.
-
Add-on features:
 - Stateless  
 - Service binding
 - Abstract Flow View and Flow Presenter Support
 - I18n Vaadin Components on FlowView

## The Presenter as the Engine of Application State

